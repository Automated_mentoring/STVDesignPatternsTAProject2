package com.epam.stv.crossplatformtesting;


import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleRegisterFactoryPage;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.LOGIN_URL;
import static com.epam.stv.locators.LocProjectLocators.START_URL;
import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;

/**
 * Created by Tatiana_Sauchanka on 3/27/2017.
 */
public class CrossBrowserTest {
    private static WebDriver driver;

    @Parameters("browser")
    @BeforeClass(description = "Start browser")

    // Passing Browser parameter from TestNG xml

    public void beforeTest(String browser) {

        // If the browser is Firefox, then do this
        if (browser.equalsIgnoreCase("chrome")) {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            driver = new ChromeDriver(capabilities);

        }

       else if (browser.equalsIgnoreCase("firefox")) {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setJavascriptEnabled(true);
            driver = new FirefoxDriver(capabilities);


        }
    }

    @BeforeClass(dependsOnMethods = "beforeTest")
    public void getURL() {
        driver.get(START_URL);
    }


    @BeforeClass(dependsOnMethods = "getURL", description = "Add implicite wait and maximize window")
    public void addImplicitly() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }


    @Test
    public void clickOnWiggleIcon() {
        new WiggleSignInFactoryPage(driver).clickOnOrangeWiggleIcon();
    }

    @Test (dependsOnMethods = {"clickOnWiggleIcon"})
    public void assertSignLink() {
        boolean b = new WiggleMainFactoryPage(driver).isSignLinkDisplayed();
        Assert.assertEquals(b, true);
    }

    @Parameters("browser")
    @Test (dependsOnMethods = {"assertSignLink"})
    public void clickOnSignLink(String browser) {
        if (browser.equalsIgnoreCase("chrome")){
            new WiggleMainFactoryPage(driver).clickOnSignLink();
        }
        else if (browser.equalsIgnoreCase("firefox")) {
            new WiggleMainFactoryPage(driver).clickOnSignLinkViaJs();
        }

    }

    @Test (dependsOnMethods = {"clickOnSignLink"})
    public void inspectLoginPage() throws InterruptedException {
        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.presenceOfElementLocated(WIGGLE_ICON_LOCATOR));
        String registerURL = new WiggleSignInFactoryPage(driver).assertCurrentURL();
        Assert.assertEquals(registerURL,LOGIN_URL);
    }


    @AfterClass
    public void afterTest() {
        // Close browser window and terminate WebDriver.
        // Doesn't work for FF48. Gecko driver issue.
        // https://github.com/SeleniumHQ/selenium/issues/2667
        driver.close();
//        driver.quit(); to be used for chrome; fails for firefox for the latest versions
    }








}
