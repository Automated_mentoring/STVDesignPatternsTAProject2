package com.epam.stv.factory.factorypages;

//import com.epam.stv.com.epam.stv.design.WiggleSignInPage;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.stv.locators.LocProjectLocators.BASKET_BUTTON_LOCATOR;

/**
 * Created by Tatiana_Sauchanka on 3/6/2017.
 */
public class WiggleMainFactoryPage extends FactoryPage {

    @FindBy (css = "#btnSignIn")
    private WebElement signLink;

    @FindBy (id = "btnJoinLink")
    private WebElement registerLink;

    @FindBy (css = "a[href='http://www.wiggle.com/cycle/']")
    private WebElement cycleTab;

    @FindBy (css = ".bem-button--primary")
    private WebElement continueShoppingButton;

    @FindBy (css = "#lcdDropdown")
    private WebElement lcdDropdown;

    @FindBy (css = "#langId")
    private  WebElement langIdDropdown;

    @FindBy (css = "#currencyId")
    private WebElement currencyDropdown;

    @FindBy (css = "#countryId")
    private WebElement deliveryDestinationDropdown;

    public WiggleMainFactoryPage(WebDriver driver){
        super(driver);
    }

    public WiggleSignInFactoryPage clickOnSignLink() {
        System.out.println("Go to Sign in page");
        signLink.click();
        return new WiggleSignInFactoryPage(driver);
    }

    public WiggleSignInFactoryPage clickOnSignLinkViaJs() {
        System.out.println("Click on Sign Link Via js");
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;
        jsExec.executeScript("document.getElementById('btnSignIn').click()");
        return new WiggleSignInFactoryPage(driver);
    }
//    Javascript
    public WiggleRegisterFactoryPage clickOnRegisterLink() {
        System.out.println("Go to Register page by js clicking on Register link");
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;
        jsExec.executeScript("document.getElementById('btnJoinLink').click()");
        return new WiggleRegisterFactoryPage(driver);
    }

    public WiggleMainFactoryPage clickOnBasketButton(){
        WebElement basketButton = driver.findElement(BASKET_BUTTON_LOCATOR);
        basketButton.click();
        return new WiggleMainFactoryPage(driver);
    }

    public WiggleMainFactoryPage clickOnLcdDropdown() {
        lcdDropdown.isDisplayed();
        lcdDropdown.click();
        return new WiggleMainFactoryPage(driver);
    }

    public WiggleMainFactoryPage clickOnLangIdDropdown() {
        langIdDropdown.isDisplayed();
        langIdDropdown.click();
        return new WiggleMainFactoryPage(driver);
    }

    public WiggleMainFactoryPage clickOnCurrencyDropdown() {
        currencyDropdown.isDisplayed();
        currencyDropdown.click();
        return new WiggleMainFactoryPage(driver);
    }

    public WiggleMainFactoryPage clickOnDeliveryDestinationDropdown() {
        deliveryDestinationDropdown.isDisplayed();
        deliveryDestinationDropdown.click();
        return new WiggleMainFactoryPage(driver);
    }

    public boolean isRegisterLinkDisplayed(){
        return registerLink.isDisplayed();
    }

    public boolean isSignLinkDisplayed(){
        return signLink.isDisplayed();
    }

//    Action
    public WiggleMainFactoryPage moveToOverCycleTab() {
        System.out.println("Mouse over Cycle item from the menu using Actions");
        new Actions(driver).moveToElement(cycleTab).build().perform();
        return new WiggleMainFactoryPage(driver);
    }

    public boolean isCycleSubmenuDisplayed(){
//        WebDriverWait wait = new WebDriverWait(driver, 10);

//        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("someid")));
        WebElement dynamicElement = (new WebDriverWait(driver, 30))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@data-page-area='MegaMenu']/li[2]")));
        return dynamicElement.isDisplayed();
    }

    public boolean isBasketButtonDisplayed(){
        WebElement basketButton = driver.findElement(BASKET_BUTTON_LOCATOR);
        return basketButton.isDisplayed();
    }

    public boolean isContinueShoppingButtonDisplayed(){
        return continueShoppingButton.isDisplayed();
    }

}
