package com.epam.stv.factory.factorytests;

import com.epam.stv.factory.business_objects.User;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import org.testng.Assert;
import org.testng.annotations.Test;


import com.epam.stv.loginpage.LoginPageValidater;

import static com.epam.stv.locators.LocProjectLocators.*;

/**
 * Created by Tatiana_Sauchanka on 2/17/2017.
 */
public class LoginFactoryTest extends BasicFactoryTest {
    @Test
    public void assertWiggleIconIsDisplayed() {
        boolean b = new WiggleSignInFactoryPage(driver).isWiggleIconDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"clickOnRegisterButton"})
    public void changePasswordButtonBackground(){
        new WiggleSignInFactoryPage(driver).highlightRegisterButton();
    }

    @Test (dependsOnMethods = {"assertRegisterButtonIsDisplayed"})
    public void clickOnRegisterButton(){
        new WiggleSignInFactoryPage(driver).pressRegisterButton();
    }

    @Test (dependsOnMethods = {"changePasswordButtonBackground"})
    public void assertValidationErrorIsDisplayed() {
        boolean b = new WiggleSignInFactoryPage(driver).isValidationErrorDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"assertWiggleIconIsDisplayed"})
    public void assertRegisterButtonIsDisplayed() {
        boolean b = new WiggleSignInFactoryPage(driver).isRegisterButtonDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"inputUserCredentials"}, description = "Make password chars visible")
    public void clickOnShowPassword(){
        new WiggleSignInFactoryPage(driver).clickOnShowPassword();
    }

    @Test (dependsOnMethods = {"assertValidationErrorIsDisplayed"})
    public void inputUserCredentials() {
        new WiggleSignInFactoryPage(driver).inputUserCredentials(new User());
    }

    @Test (dependsOnMethods = {"clickOnShowPassword"})
    public void assertLoginButtonIsDisplayed() {
        boolean b = new WiggleSignInFactoryPage(driver).isLoginButtonDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test(dependsOnMethods = {"assertLoginButtonIsDisplayed"})
    public void assertLoginButtonLable() {
        String loginLable = new WiggleSignInFactoryPage(driver).lableText();
        LoginPageValidater loginPageValidater = new LoginPageValidater();
        boolean b = loginPageValidater.registrExpValidate(loginLable);
        Assert.assertEquals(b, true);
    }

    @Test(dependsOnMethods = {"assertLoginButtonLable"})
    public void clickLoginButton() {
        new WiggleSignInFactoryPage(driver).clickLoginButton();
    }

    @Test(dependsOnMethods = {"clickLoginButton"})
    public void assertWarningAppears() {
        boolean b = new WiggleSignInFactoryPage(driver).isWarningMessageDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test(dependsOnMethods = {"assertWarningAppears"})
    public void assertWarningMessageText() {
        String warningText = new WiggleSignInFactoryPage(driver).warningMessageText();
        Assert.assertEquals(warningText, WARNING_TEXT);
    }

}
