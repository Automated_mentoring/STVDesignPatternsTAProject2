package com.epam.stv.factorymethod;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.epam.stv.locators.LocProjectLocators.START_URL;
import static com.epam.stv.locators.LocProjectLocators.WIGGLE_ICON_LOCATOR;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class BasicFactoryMethodTest {
    private WebDriverCreator creator = new ChromeDriverCreator();
    protected WebDriver driver = creator.driverFactoryMethod();


    @BeforeClass(description = "Start browser")
    public void setUp() {
//  create Chrome driver instance

        driver.get(START_URL);
        (new WebDriverWait(driver, 5)).
                until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver webDriver) {
                        return webDriver.findElement(WIGGLE_ICON_LOCATOR);

                    }
                });
    }

    @BeforeClass(dependsOnMethods = "setUp", description = "Add implicite wait and maximize window")
    public void addImplicitly() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("D:\\STVActionsRemoteGridProject\\test-output\\testScreenShot.jpg"));

        }
    }
//
//    @AfterClass
//    public void afterClass() throws Exception {
//        driver.quit();
//    }



}
