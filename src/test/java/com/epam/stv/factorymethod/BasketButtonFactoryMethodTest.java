package com.epam.stv.factorymethod;

import com.epam.stv.factory.factorypages.WiggleMainFactoryPage;
import com.epam.stv.factory.factorypages.WiggleRegisterFactoryPage;
import com.epam.stv.factory.factorypages.WiggleSignInFactoryPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class BasketButtonFactoryMethodTest extends BasicFactoryMethodTest {

    @Test
    public void clickOnWiggleIcon() {
        new WiggleSignInFactoryPage(driver).clickOnOrangeWiggleIcon();
    }

    @Test (dependsOnMethods = {"clickOnWiggleIcon"})
    public void assertBasketButton() {
        boolean b = new WiggleMainFactoryPage(driver).isBasketButtonDisplayed();
        Assert.assertEquals(b, true);
    }

    @Test (dependsOnMethods = {"assertBasketButton"})
    public void clickOnBasketButton(){
        new WiggleMainFactoryPage(driver).clickOnBasketButton();
    }

    @Test (dependsOnMethods = {"clickOnBasketButton"})
    public void assertBasketPage(){
        boolean b = new WiggleMainFactoryPage(driver).isContinueShoppingButtonDisplayed();
        Assert.assertEquals(b, true);
    }



}
