package com.epam.stv.factorymethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;
import java.io.IOException;

/**
 * Created by Tatiana_Sauchanka on 4/11/2017.
 */
public class ChromeDriverCreator extends WebDriverCreator {
    @Override
    public WebDriver driverFactoryMethod() {
//        String exePath = "C:\\Chromedriver\\chromedriver.exe";
//        System.setProperty("webdriver.chrome.driver", exePath);
//        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//        capabilities.setJavascriptEnabled(true);
//        driver = new ChromeDriver(capabilities);

        ChromeDriverService service = new ChromeDriverService.Builder().usingDriverExecutable(
                new File("C:\\Chromedriver\\chromedriver.exe")).build();
        try {
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver = new ChromeDriver(service);
        return driver;
    }
}
