package com.epam.stv.loginpage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static sun.awt.image.PixelConverter.Argb.instance;

/**
 * Created by Tatiana_Sauchanka on 2/19/2017.
 */
public class LoginPageValidater {

    public boolean registrExpValidate(String buttonName){
        Pattern p = Pattern.compile("[a-zA-Z ]*");
        Matcher m = p.matcher(buttonName);
        boolean b = m.matches();
        return b;
    }

    public void highlightElement(WebDriver driver, WebElement element)
    {
        String bg = element.getCssValue("backgroundColor");
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("arguments[0].style.backgroundColor = '" + "violet" + "'", element);
// take screenshot here
//        js.executeScript("arguments[0].style.backgroundColor = '" + bg + "'", element);
    }


}
